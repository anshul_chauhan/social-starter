package com.lembas.tika;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import com.lembas.social.PatternMatcherGroupHtml;

public class HtmlParse {

	public static void main(final String[] args) throws IOException, SAXException, TikaException {

		// detecting the file type
		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		ParseContext pcontext = new ParseContext();
		HtmlParser htmlparser = new HtmlParser();
		// for (int i = 0; i < 100; i++) {

		/*
		 * FileInputStream inputstream = new FileInputStream( new
		 * File(HtmlParse.class.getClassLoader().getResource("test.html").
		 * getPath()));
		 */
		long start = System.currentTimeMillis();
		Pattern p = Pattern.compile("[0-9]*\\.?[0-9]+", Pattern.DOTALL);
		// Html parser
		for (int i = 0; i < 100; i++) {
			InputStream is = new ByteArrayInputStream(PatternMatcherGroupHtml.stringToSearch.getBytes());
			htmlparser.parse(is, handler, metadata, pcontext);
			// handler = new BodyContentHandler();
			String stringToSearch = handler.toString();
			// System.out.println(stringToSearch+ " "+i);
			int indexOfCredited = stringToSearch.indexOf("credited");
			Matcher m = p.matcher(stringToSearch.substring(indexOfCredited, indexOfCredited + 20));
			if (m.find()) {
				String cr = m.group(0);
			//	System.out.println("Credit: " + cr);
			}
			int indexOfDebited = stringToSearch.indexOf("debited", indexOfCredited);
			m = p.matcher(stringToSearch.substring(indexOfDebited, indexOfDebited + 20));
			if (m.find()) {
				String db = m.group(0);
				//System.out.println("Debit: " + db);
			}
			handler = new BodyContentHandler();
		}
		System.out.println("Total time: " + (System.currentTimeMillis() - start));
		// System.out.println("Contents of the document:" + handler.toString());
		// System.out.println("Metadata of the document:");
		// String[] metadataNames = metadata.names();
		//
		// for (String name : metadataNames) {
		// System.out.println(name + ": " + metadata.get(name));
		// }
	}
}