package com.lembas.tika;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

public class PostFile {
	public static void main(String[] args) throws Exception {
		// apacheHttp();
		func();
	}

	private static void apacheHttp() throws IOException, ClientProtocolException {
		HttpClient httpClient = new DefaultHttpClient();
		StringBuilder result = new StringBuilder();
		HttpPut putRequest = new HttpPut("http://localhost:9998/tika");
		putRequest.addHeader("Content-Type", "application/pdf");
		putRequest.addHeader("Accept", "text/plain");
		FileEntity fileEntity = new FileEntity(
				new File(PDFParse.class.getClassLoader().getResource("Invoice_Flipkart.pdf").getPath()));
		putRequest.setEntity(fileEntity);
		HttpResponse response = httpClient.execute(putRequest);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
		String output;
		while ((output = br.readLine()) != null) {
			result.append(output);
		}
		System.out.println(result.toString());
	}

	public static void func() throws Exception {
		final InputStream stream = new FileInputStream(
				new File(PDFParse.class.getClassLoader().getResource("image.jpg").getPath()));
		final byte[] bytes = new byte[stream.available()];
		stream.read(bytes);
		stream.close();
		com.mashape.unirest.http.HttpResponse<String> asJson = Unirest.put("http://localhost:9998/tika")
				.header("accept", "text/plain").header("X-Tika-OCRLanguage","eng").header("Content-type", "image/jpeg").body(bytes).asString();
		String string = asJson.getBody();
		System.out.println(string);
	}
}
