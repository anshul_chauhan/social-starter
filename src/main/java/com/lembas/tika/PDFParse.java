package com.lembas.tika;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.mashape.unirest.http.Unirest;

public class PDFParse {
	public static void main(final String[] args) throws Exception {

		BodyContentHandler handler = new BodyContentHandler();
		Metadata metadata = new Metadata();
		String path = PDFParse.class.getClassLoader().getResource("invoice.pdf").getPath();

		ParseContext pcontext = new ParseContext();
		PDFParser pdfparser = new PDFParser();
		HttpClient httpClient = new DefaultHttpClient();
		final InputStream stream = new FileInputStream(new File(path));
		final byte[] bytes = new byte[stream.available()];
		stream.read(bytes);
		stream.close();

		long start = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			FileInputStream inputstream = new FileInputStream(new File(path));

			// parsing the document using PDF parser
			pdfparser.parse(inputstream, handler, metadata, pcontext);
			// handler.toString();
			// getting the content of the document
			System.out.println(handler.toString());
			handler = new BodyContentHandler();
		}
		long apache = System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			javaRest(bytes);
		}
		// System.out.println(
		// "Total time diff=====> Apache: " + apache + " IText: " +
		// (System.currentTimeMillis() - start));
		long itext = System.currentTimeMillis() - start;
		start = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			parseTikeService(new ByteArrayInputStream(bytes), bytes.length, httpClient);
		}
		System.out.println("Total time diff=====> Apache: " + apache + "  JavaRest: " + itext + " Service: "
				+ (System.currentTimeMillis() - start));
	}

	public static void parsePdf(String pdf) throws IOException {
		PdfReader reader = new PdfReader(pdf);
		System.out.println(PdfTextExtractor.getTextFromPage(reader, 1));
	}

	public static String parseTikeService(InputStream is, long length, HttpClient httpClient)
			throws ClientProtocolException, IOException {

		StringBuilder result = new StringBuilder();
		HttpPut putRequest = new HttpPut("http://localhost:9998/tika");
		putRequest.addHeader("Content-Type", "text/html");
		putRequest.addHeader("Accept", "text/plain");
		InputStreamEntity entity = new InputStreamEntity(is, length);
		// FileEntity fileEntity = new FileEntity(new File(path));
		putRequest.setEntity(entity);
		HttpResponse response = httpClient.execute(putRequest);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
		String output;
		while ((output = br.readLine()) != null) {
			result.append(output);
		}
		br.close();
		return result.toString();
	}

	public static void uniRest(byte[] bytes) throws Exception {

		com.mashape.unirest.http.HttpResponse<String> asJson = Unirest.put("http://localhost:9998/tika")
				.header("accept", "text/plain").header("Content-Type", "application/pdf").body(bytes).asString();
		System.out.println(asJson.getBody());
	}

	public static void javaRest(byte[] bytes) {
		try {
			URL url = new URL("http://localhost:9998/tika");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Content-Type", "application/pdf");
			connection.setRequestProperty("Accept", "text/plain");
			OutputStream os = connection.getOutputStream();
			os.write(bytes);
			os.flush();
			os.close();
			BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;

			while ((inputLine = input.readLine()) != null) {
				System.out.println(inputLine);
			}

			input.close();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
