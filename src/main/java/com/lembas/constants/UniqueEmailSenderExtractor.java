package com.lembas.constants;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Lists;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.google.common.collect.Maps;

public class UniqueEmailSenderExtractor {
	/** Application name. */
	private static final String APPLICATION_NAME = "Gmail API Java Quickstart";
	private static Map<String, Integer> emailAddresses = Maps.newHashMap();
	/** Directory to store user credentials for this application. */
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"),
			".credentials/gmail-java-quickstart.json");

	/** Global instance of the {@link FileDataStoreFactory}. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;
	// Create file
	FileWriter fstream = null;

	/**
	 * Global instance of the scopes required by this quickstart.
	 *
	 * If modifying these scopes, delete your previously saved credentials at
	 * ~/.credentials/gmail-java-quickstart.json
	 */
	private static final List<String> SCOPES = Arrays.asList(GmailScopes.GMAIL_READONLY);

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @return an authorized Credential object.
	 * @throws IOException
	 */
	public static Credential authorize() throws IOException {
		// Load client secrets.
		InputStream in = UniqueEmailSenderExtractor.class.getResourceAsStream("/client_secret.json");
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						/* .setDataStoreFactory(DATA_STORE_FACTORY) */.setAccessType("offline").build();
		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
		System.out.println("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
		return credential;
	}

	/**
	 * Build and return an authorized Gmail client service.
	 * 
	 * @return an authorized Gmail client service
	 * @throws IOException
	 */
	public static Gmail getGmailService() throws IOException {
		Credential credential = authorize();
		return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	public static void main(String[] args) throws IOException {
		// Build a new authorized API client service.

		Gmail service = getGmailService();

		// Print the labels in the user's account.
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				try {
				// String firstHeaderStringValue =
				// responseHeaders.getFirstHeaderStringValue("From");
				// System.out.println(message.getSnippet());
				List<MessagePartHeader> headers = message.getPayload().getHeaders();
				for (MessagePartHeader header : headers) {

					if (header.getName().equals("From")) {
						String address = header.getValue();
						if (address.contains("<"))
							address = address.substring(address.lastIndexOf("<") + 1, address.indexOf(">"));
						address = address.toLowerCase();
						System.out.println(address);
						if (!emailAddresses.containsKey(address)) {

							emailAddresses.put(address, new Integer(1));
						} else {
							Integer count = emailAddresses.get(address);
							emailAddresses.put(address, count + 1);
						}
					}
				}
				}catch(Exception e) {
					e.printStackTrace();
				}
				// List<MessagePart> parts = message.getPayload().getParts();
				// for (MessagePart part : parts) {
				// if (part.getMimeType().equals("text/plain")) {
				// System.out.println(new String(part.getBody().decodeData()));
				// }
				// }
				// System.out.println(decodeData);
				// byte[] emailBytes = Base64.decodeBase64(message.getRaw());
				// Properties props = new Properties();
				// Session session = Session.getDefaultInstance(props, null);
				//
				// MimeMessage email = new MimeMessage(session, new
				// ByteArrayInputStream(emailBytes));

				// System.out.println(emailBytes);
			}

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				System.out.println("Error Message: " + e.getMessage());
			}
		};
		String user = "me";

		BatchRequest batch = service.batch();
		service.users().messages().list(user);
		List<String> labels = Lists.newArrayListWithCapacity(1);
		/* .setQ("from:no-reply@paytm.com") */
		labels.add("INBOX");
		ListMessagesResponse messageList = service.users().messages().list(user).setMaxResults(100L).setLabelIds(labels)
				.execute();
		// String nextPageToken = messages.getNextPageToken();
		do {
			List<Message> messages = messageList.getMessages();
			List<String> metaHeaders = Lists.newArrayListWithCapacity(1);
			metaHeaders.add("From");
			for (Message label : messages) {
				service.users().messages().get(user, label.getId()).setFormat("metadata")
						.setMetadataHeaders(metaHeaders).queue(batch, callback);
			}
			batch.execute();
			messageList = service.users().messages().list(user).setMaxResults(100L)
					.setPageToken(messageList.getNextPageToken()).execute();
		} while (StringUtils.isNotEmpty(messageList.getNextPageToken()));

		try {
			// Create file
			FileWriter fstream = new FileWriter("addresses.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			// Write to file
			for (String address : emailAddresses.keySet()) {
				if (address != null) {
					out.write(emailAddresses.get(address) + " " + address.toString());
					out.newLine();
				}
			}

			// Close the output stream
			out.close();
		} catch (Exception e) {
			// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
		System.out.println("=================Script Finished========================");
		// System.out.println(messages);
		// messages.list(arg0)
		// ListLabelsResponse listResponse =
		// service.users().labels().list(user).execute();
		// List<Label> labels = listResponse.getLabels();
		// if (labels.size() == 0) {
		// System.out.println("No labels found.");
		// } else {
		// System.out.println("Labels:");
		// for (Message label : labels) {
		// System.out.printf("- %s\n", label.toPrettyString());
		// }
		// }
	}

}