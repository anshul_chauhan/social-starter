package com.lembas.constants;

public class Constants {
	public static final String PAYTM_SUMMARY = "PAYTM_SUMMARY";
	public static final String FLIPKART_DELIVERED = "FLIPKART_DELIVERED";
	public static final String FLIPKART_RETURNED = "FLIPKART_RETURNED";
	public static final String AMAZON_DELIVERED = "AMAZON_DELIVERED";
	public static final String AMAZON_RETURNED = "AMAZON_RETURNED";
	public static final String SNAPDEAL_DELIVERED = "SNAPDEAL_DELIVERED";
	public static final String SNAPDEAL_RETURNED = "SNAPDEAL_RETURNED";
	public static final String PAYTM_DELIVERED = "PAYTM_DELIVERED";
	public static final String PAYTM_RETURNED = "PAYTM_RETURNED";
	public static final String HDFC_CREDIT_CARD = "HDFC_CREDIT_CARD";
	public static final String AMEX_CREDIT_CARD = "AMEX_CREDIT_CARD";
	public static final String KOTAK_CREDIT_CARD = "KOTAK_CREDIT_CARD";
	public static final String ICICI_CREDIT_CARD = "ICICI_CREDIT_CARD";
	public static final String HSBC_CREDIT_CARD = "HSBC_CREDIT_CARD";
	public static final String CITI_CREDIT_CARD = "CITI_CREDIT_CARD";
	public static final String SBI_CREDIT_CARD = "SBI_CREDIT_CARD";
	public static final String YES_CREDIT_CARD = "YES_CREDIT_CARD";
	public static final String INDUSIND_CREDIT_CARD = "INDUSIND_CREDIT_CARD";
	public static final String RBL_CREDIT_CARD = "INDUSIND_CREDIT_CARD";
	public static final String SC_CREDIT_CARD = "SC_CREDIT_CARD";
	public static final String AIRTEL_POST_PAID = "AIRTEL_POST_PAID";
	public static final String VODAFONE_POST_PAID = "VODAFONE_POST_PAID";
	public static final String IDEA_POST_PAID = "IDEA_POST_PAID";
	public static final String AIRCEL_POST_PAID = "AIRCEL_POST_PAID";
                   
	// INNER CONST ANTS
	public static final String PAYTM_SUMMARY_CREDITED = "PAYTM_CREDITED";
	public static final String PAYTM_SUMMARY_DEBITED = "PAYTM_DEBITED";
	public static final String DATE = "DATE";
	public static final String ORDER_TOTAL = "ORDER_TOTAL";
	public static final String INTERNAL_DATE = "internalDate";
	public static final String NAME = "NAME";
	public static final String PROFILE_NAME="PROFILE_NAME";
	public static final String ADDRESS = "ADDRESS";
	public static final String PHONE = "PHONE";
	
	//properties
	public static final long LIMIT = 50;
}
