package com.lembas.social;

import static com.lembas.constants.Constants.AMAZON_DELIVERED;
import static com.lembas.constants.Constants.FLIPKART_DELIVERED;
import static com.lembas.constants.Constants.ORDER_TOTAL;
import static com.lembas.constants.Constants.SNAPDEAL_DELIVERED;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.Oauth2Scopes;
import com.google.api.services.oauth2.model.Userinfoplus;
import com.google.common.collect.Maps;
import com.lembas.social.parsers.FlipkartReturnParser;

public class MailExtractorServer {
	/** Application name. */
	private static final String APPLICATION_NAME = "Gmail API Java Quickstart";
	/** Directory to store user credentials for this application. */
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"),
			".credentials/gmail-java-quickstart.json");

	/** Global instance of the {@link FileDataStoreFactory}. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;
	// Create file
	FileWriter fstream = null;

	private static final List<String> LABELS = Arrays.asList("INBOX");
	/**
	 * Global instance of the scopes required by this quickstart.
	 *
	 * If modifying these scopes, delete your previously saved credentials at
	 * ~/.credentials/gmail-java-quickstart.json
	 */
	private static final List<String> SCOPES = Arrays.asList(GmailScopes.GMAIL_READONLY, Oauth2Scopes.USERINFO_PROFILE);

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);

		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @return an authorized Credential object.
	 * @throws IOException
	 */
	public static Credential authorize() throws IOException {
		// Load client secrets.
		InputStream in = MailExtractorServer.class.getResourceAsStream("/client_secret.json");
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES).setDataStoreFactory(DATA_STORE_FACTORY).setAccessType("offline").build();
		// Credential credential = flow.loadCredential("user");
		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
		// credential.getClientAuthentication()
		// System.out.println("Credentials saved to " +
		// DATA_STORE_DIR.getAbsolutePath());
		return credential;
	}

	/**
	 * Build and return an authorized Gmail client service.
	 * 
	 * @return an authorized Gmail client service
	 * @throws IOException
	 */
	public static Gmail getGmailService() throws IOException {
		Credential credential = authorize();
		return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	public static void printProfile() throws IOException {
		Credential credential = authorize();
		Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential)
				.setApplicationName("Oauth2").build();
		Userinfoplus userinfo = oauth2.userinfo().get().execute();
		System.out.println(userinfo.toPrettyString());
	}

	public static void main(String[] args) throws IOException, ParseException {
		// Build a new authorized API client service.

		Gmail service = getGmailService();
		String user = "me";
		Map<String, List<Data>> userTransactionData = Maps.newHashMap();
		long start = System.currentTimeMillis();
		//printProfile();
//		PaytmSummaryParser.parseSummary(service, user, userTransactionData);
//		System.out.println("Time taken===>Paytm Summary: " + (System.currentTimeMillis() - start));
//		start = System.currentTimeMillis();

//		SnapdealParser.parseOrder(service, user, userTransactionData);
//		System.out.println("Time taken===>Snapdeal: " + (System.currentTimeMillis() - start));
//		start = System.currentTimeMillis();

//		FlipkartParser.parseOrder(service, user, userTransactionData);
//		System.out.println("Time taken===>Flipkart: " + (System.currentTimeMillis() - start));
//		start = System.currentTimeMillis();
//
//		AmazonParser.parseShipment(service, user, userTransactionData);
//		System.out.println("Time taken===>Amazon: " + (System.currentTimeMillis() - start));
//		start = System.currentTimeMillis();
//
//		PaytmOrder.parseOrder(service, user, userTransactionData);
//		System.out.println("Time taken===>Paytm Delivered: " + (System.currentTimeMillis() - start));
//		System.out.println("====Script Finished====");
		
//		AmazonReturnParser.parseShipment(service, user, userTransactionData);
//		System.out.println("Time taken===>Amazon Returns: " + (System.currentTimeMillis() - start));
//		start = System.currentTimeMillis();
		
		FlipkartReturnParser.parseShipment(service, user, userTransactionData);
		System.out.println("Time taken===>Flipkart Returns: " + (System.currentTimeMillis() - start));
		start = System.currentTimeMillis();

		long positive = 0, negative = 0;
//		List<Data> list = userTransactionData.get(PAYTM_SUMMARY);
//		for (Data data : list) {
//			positive += NumberUtils.toFloat(data.getProperties().get(PAYTM_SUMMARY_CREDITED), 0);
//			negative += NumberUtils.toFloat(data.getProperties().get(PAYTM_SUMMARY_DEBITED), 0);
//		}
//		System.out.println("Paytm Summary Credited: " + positive + " Debited: " + negative);
//		positive = 0;
		for (Data data : userTransactionData.get(SNAPDEAL_DELIVERED)) {
			positive += NumberUtils.toFloat(data.getProperties().get(ORDER_TOTAL), 0);
		}
		System.out.println("Snapdeal Delivered Total: " + positive);
		positive = 0;
		for (Data data : userTransactionData.get(FLIPKART_DELIVERED)) {
			positive += NumberUtils.toFloat(data.getProperties().get(ORDER_TOTAL), 0);
		}
		System.out.println("Flipkart Delivered Total: " + positive);
		positive = 0;
		for (Data data : userTransactionData.get(AMAZON_DELIVERED)) {
			Number parse = NumberFormat.getNumberInstance(java.util.Locale.US)
					.parse(data.getProperties().get(ORDER_TOTAL));
			positive += parse.floatValue();
		}
		System.out.println("Amazon Delivered Total: " + positive);
	}

}