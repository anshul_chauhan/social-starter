package com.lembas.social.parsers;

import static com.lembas.constants.Constants.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.cxf.common.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.internal.Sets;
import com.google.api.client.googleapis.batch.BatchCallback;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonErrorContainer;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Base64;
import com.google.api.client.util.Lists;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.lembas.social.Data;
import com.lembas.social.ParsingUtils;

public class SnapdealParser {
	private static final String ORDER_DELIVERED_EMAIL = "from:noreply@snapdeals.co.in subject:(\"have been delivered\" OR \"has been delivered\" OR \"Delivery Confirmation\") -experience to:me";
	private final static Logger logger = LoggerFactory.getLogger(SnapdealParser.class);
	private static Pattern p = Pattern.compile("[0-9]*\\.?[0-9]+", Pattern.DOTALL);

	public static void parseOrder(Gmail service, String user, Map<String, List<Data>> userTransactionData)
			throws IOException {
		BatchRequest batch = service.batch();
		ListMessagesResponse messageList = service.users().messages().list(user).setQ(ORDER_DELIVERED_EMAIL)
				.setMaxResults(LIMIT).execute();

		BatchRequest attachBatch = service.batch();
		List<Message> messages = messageList.getMessages();
		Set<String> unParsedOrderIds = Sets.newHashSet();
		if (messages != null) {
			for (Message message : messages) {
				service.users().messages().get(user, message.getId()).queue(batch,
						getSnapdealOrderCallback(service, userTransactionData, attachBatch, unParsedOrderIds));
			}
			batch.execute();
			if (!CollectionUtils.isEmpty(unParsedOrderIds)) {
				StringBuilder query = new StringBuilder();
				query.append("\"is confirmed\" AND (");
				for (String orderId : unParsedOrderIds) {
					query.append(orderId);
					query.append(" OR ");
				}
				query.setLength(query.length() - 4);
				query.append(")");
				messageList = service.users().messages().list(user).setQ(query.toString()).execute();
				messages = messageList.getMessages();
				if (messages != null) {
					for (Message message : messages) {
						service.users().messages().get(user, message.getId()).queue(attachBatch,
								getSnapdealOrderConfirmCallback(userTransactionData));
					}
					attachBatch.execute();
				}
			}
			System.out.println(userTransactionData);
			// attachBatch.execute();
		}
	}

	private static JsonBatchCallback<Message> getSnapdealOrderCallback(final Gmail service,
			final Map<String, List<Data>> userTransactionData, BatchRequest attachBatch, Set<String> unParsedOrderIds) {
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				MessagePart payload = message.getPayload();
				List<MessagePart> parts = payload.getParts();
				String id = message.getId();
				List<MessagePartHeader> headers = message.getPayload().getHeaders();
				for (MessagePartHeader messagePartHeader : headers) {
					if (messagePartHeader.getName().equals("Subject")) {
						String value = messagePartHeader.getValue();
						if (value.startsWith("Your ")) {
							// Newer message
							if (parts != null) {
								for (MessagePart part : parts) {

									if (part.getMimeType().startsWith("multipart/alternative")) {
										List<MessagePart> messageParts = part.getParts();
										for (MessagePart msgPart : messageParts) {
											if (msgPart.getMimeType().equals("text/html")) {
												parseOrderId(msgPart.getBody().decodeData(),
														(String) message.get(INTERNAL_DATE), userTransactionData,
														unParsedOrderIds);
												return;
											}
										}
									}
								}
							} else if (payload.getBody() != null) {
								parseOrderHtml(payload.getBody().decodeData(), (String) message.get(INTERNAL_DATE),
										userTransactionData, id);
								return;
							}

						} else {
							// Older message
							if (parts != null) {
								for (MessagePart part : parts) {
									if (part.getMimeType().startsWith("multipart/alternative")) {
										List<MessagePart> messageParts = part.getParts();
										for (MessagePart msgPart : messageParts) {
											if (msgPart.getMimeType().equals("text/html")) {
												parseOrderHtml(msgPart.getBody().decodeData(),
														(String) message.get(INTERNAL_DATE), userTransactionData, id);
												return;
											}
										}
									}
								}
							} else if (payload.getBody() != null) {
								parseOrderHtml(payload.getBody().decodeData(), (String) message.get(INTERNAL_DATE),
										userTransactionData, id);
								return;
							} else {
								System.out.println();
							}
						}
					}

				}
			};

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				logger.error("Error Message: " + e.getMessage());
			}
		};
		return callback;
	}

	private static JsonBatchCallback<Message> getSnapdealOrderConfirmCallback(
			final Map<String, List<Data>> userTransactionData) {
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				MessagePart payload = message.getPayload();
				List<MessagePart> parts = payload.getParts();
				String id = message.getId();
				List<MessagePartHeader> headers = message.getPayload().getHeaders();
				for (MessagePartHeader messagePartHeader : headers) {
					if (messagePartHeader.getName().equals("Subject")) {

						if (parts != null) {
							for (MessagePart part : parts) {
								if (part.getMimeType().startsWith("multipart/alternative")) {
									List<MessagePart> messageParts = part.getParts();
									for (MessagePart msgPart : messageParts) {
										if (msgPart.getMimeType().equals("text/html")) {
											parseConfirmOrderHtml(msgPart.getBody().decodeData(),
													(String) message.get(INTERNAL_DATE), userTransactionData, id);
											return;
										}
									}
								}
							}
						} else if (payload.getBody() != null) {
							parseOrderHtml(payload.getBody().decodeData(), (String) message.get(INTERNAL_DATE),
									userTransactionData, id);
							return;
						} else {
							System.out.println("Message not recognized(Snapdeal). ID: " + id);
						}
					}

				}
			};

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				logger.error("Error Message: " + e.getMessage());
			}
		};
		return callback;
	}

	private static String parseOrderId(byte[] bytes, String timeStamp, Map<String, List<Data>> userTransactionData,
			Set<String> unParsedOrderIds) {
		BufferedReader input = ParsingUtils.readTextFromHtml(bytes);
		String newId = "";
		try {
			String inputLine;

			while ((inputLine = input.readLine()) != null) {
				if (inputLine.indexOf("Order ID") >= 0) {
					Matcher m = p.matcher(inputLine);
					if (m.find()) {
						String cr = m.group(0);
						if (unParsedOrderIds.add(cr)) {
							newId = cr;
						}
						break;
					}

				}
			}

			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return newId;
	}

	private static void parseOrderHtml(byte[] bytes, String timeStamp, Map<String, List<Data>> userTransactionData,
			String id) {
		BufferedReader input = ParsingUtils.readTextFromHtml(bytes);
		try {
			String inputLine;
			Data data = new Data();
			data.setDataType(SNAPDEAL_DELIVERED);
			Pattern mobile = Pattern.compile("(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}");
			boolean foundAddress = false;
			String address = "";
			String number = "";
			String paidAmount = "";
			String name = "";
			while ((inputLine = input.readLine()) != null) {
				if (inputLine.indexOf("Paid Amount") >= 0) {
					Matcher m = p.matcher(inputLine);
					if (m.find()) {
						paidAmount = m.group(0);
						// break;
					}

				} else if (inputLine.indexOf("DELIVERY ADDRESS") >= 0 || inputLine.indexOf("Shipping Address") >= 0) {
					foundAddress = true;
				} else if (foundAddress) {
					Matcher m = mobile.matcher(inputLine);
					if (m.find()) {
						number = m.group(0);
						number = number.substring(number.length() - 10);
						foundAddress = false;
					} else if (StringUtils.isNotBlank(inputLine)) {
						if (name.equals("")) {
							name = inputLine.trim();
						} else {
							address += inputLine.trim() + " ";
						}
					}

				}
			}
			data.addProperty(DATE, timeStamp);
			data.addProperty(PHONE, number);
			data.addProperty(ADDRESS, address);
			data.addProperty(NAME, name);
			data.addProperty("ID", id);
			if (StringUtils.isEmpty(paidAmount)) {
				System.out.println("Order total not found!!");
			} else {
				data.addProperty(ORDER_TOTAL, paidAmount);
			}
			List<Data> list = userTransactionData.get(SNAPDEAL_DELIVERED);
			if (list == null)
				list = Lists.newArrayList();
			list.add(data);
			userTransactionData.put(SNAPDEAL_DELIVERED, list);

			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void parseConfirmOrderHtml(byte[] bytes, String timeStamp,
			Map<String, List<Data>> userTransactionData, String id) {
		BufferedReader input = ParsingUtils.readTextFromHtml(bytes);
		try {
			String inputLine;
			Data data = new Data();
			data.setDataType(SNAPDEAL_DELIVERED);
			Pattern mobile = Pattern.compile("(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}");
			boolean foundAddress = false;
			String address = "";
			String number = "";
			String paidAmount = "";
			String name = "";
			while ((inputLine = input.readLine()) != null) {
				if (inputLine.indexOf("Total Price") >= 0) {
					//Will figure out regex later on 
					int beginIndex = inputLine.indexOf("Rs.")+3;
					paidAmount = inputLine.substring(beginIndex);
					paidAmount = paidAmount.trim();

				} else if (inputLine.indexOf("Delivery Address") >= 0 || inputLine.indexOf("Shipping Address") >= 0) {
					foundAddress = true;
					continue;
				} else if (inputLine.indexOf("Payment Type") >= 0) {
					System.out.println(inputLine);
				} else if (inputLine.indexOf("Phone Number") >= 0) {
					foundAddress = false;
					number = "f";
					continue;
				}

				if (foundAddress) {
					if (StringUtils.isNotBlank(inputLine)) {
						if (name.equals("")) {
							name = inputLine.trim();
						} else {
							address += inputLine.trim() + " ";
						}

					}
				} else if (number.equals("f")) {
					Matcher m = mobile.matcher(inputLine);
					if (m.find()) {
						number = m.group(0);
						number = number.substring(number.length() - 10);
					}
				}
			}
			data.addProperty(DATE, timeStamp);
			data.addProperty(PHONE, number);
			data.addProperty(ADDRESS, address);
			data.addProperty(NAME, name);
			data.addProperty("ID", id);
			if (StringUtils.isEmpty(paidAmount)) {
				System.out.println("Order total not found!!");
			} else {
				data.addProperty(ORDER_TOTAL, paidAmount);
			}
			List<Data> list = userTransactionData.get(SNAPDEAL_DELIVERED);
			if (list == null)
				list = Lists.newArrayList();
			list.add(data);
			userTransactionData.put(SNAPDEAL_DELIVERED, list);

			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	// Uncomment in case we decide to parse invoices
	// private static BatchCallback<MessagePartBody, GoogleJsonErrorContainer>
	// getSnapdealInvoiceCallback(
	// final Gmail service, final Map<String, List<Data>> userTransactionData,
	// String date) {
	// BatchCallback<MessagePartBody, GoogleJsonErrorContainer> callback = new
	// BatchCallback<MessagePartBody, GoogleJsonErrorContainer>() {
	//
	// @Override
	// public void onSuccess(MessagePartBody attachPart, HttpHeaders
	// responseHeaders) throws IOException {
	// byte[] bytes = Base64.decodeBase64(attachPart.getData());
	// parseInvoice(bytes, date, userTransactionData);
	//
	// }
	//
	// @Override
	// public void onFailure(GoogleJsonErrorContainer e, HttpHeaders
	// responseHeaders) throws IOException {
	// logger.error("Error Message: " + e.getError().getMessage());
	//
	// }
	//
	// };
	// return callback;
	// }
	//
	// private static void parseInvoice(byte[] bytes, String timeStamp,
	// Map<String, List<Data>> userTransactionData) {
	// BufferedReader input = ParsingUtils.readTextFromPdf(bytes);
	// try {
	// String inputLine;
	// Data data = new Data();
	// data.setDataType(SNAPDEAL_DELIVERED);
	// while ((inputLine = input.readLine()) != null) {
	// System.out.println(inputLine);
	// if (inputLine.indexOf("TOTAL") >= 0) {
	// Matcher m = p.matcher(inputLine);
	// if (m.find()) {
	// String cr = m.group(0);
	// data.addProperty(ORDER_TOTAL, cr);
	// break;
	// }
	//
	// }
	// }
	// data.addProperty(DATE, timeStamp);
	// List<Data> list = userTransactionData.get(SNAPDEAL_DELIVERED);
	// if (list == null)
	// list = Lists.newArrayList();
	// list.add(data);
	// userTransactionData.put(SNAPDEAL_DELIVERED, list);
	//
	// input.close();
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	// try {
	// input.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }
	// }

}
