package com.lembas.social.parsers;

import static com.lembas.constants.Constants.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Lists;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.lembas.social.Data;
import com.lembas.social.ParsingUtils;

public class AmazonParser {
	private static final String SHIPMENT_EMAIL = "from:ship-confirm@amazon.in";
	private final static Logger logger = LoggerFactory.getLogger(PaytmSummaryParser.class);
	private final static Pattern p = Pattern.compile("\\d{1,3}(,\\d{3})*(\\.\\d\\d)?", Pattern.DOTALL);

	public static void parseShipment(Gmail service, String user, Map<String, List<Data>> userTransactionData)
			throws IOException {
		BatchRequest batch = service.batch();
		ListMessagesResponse messageList = service.users().messages().list(user).setQ(SHIPMENT_EMAIL).setMaxResults(LIMIT)
				.execute();

		List<Message> messages = messageList.getMessages();
		for (Message message : messages) {
			service.users().messages().get(user, message.getId()).queue(batch,
					getShipmentCallback(System.currentTimeMillis(), userTransactionData));
		}
		batch.execute();
	}

	private static JsonBatchCallback<Message> getShipmentCallback(final long currentTimeMillis,
			final Map<String, List<Data>> userTransactionData) {
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				MessagePart payload = message.getPayload();
				List<MessagePart> parts = payload.getParts();
				for (MessagePart part : parts) {
					if (part.getMimeType().startsWith("multipart")) {
						List<MessagePart> messageParts = part.getParts();
						for (MessagePart msgPart : messageParts) {
							if (msgPart.getMimeType().equals("text/html")) {
								// long time = System.currentTimeMillis();
								parse(msgPart.getBody().decodeData(), (String) message.get(INTERNAL_DATE),
										userTransactionData);
								// System.out.println(
								// "Time by paytm parser============>" +
								// (System.currentTimeMillis() - time));
							}
						}
					}
				}
			}

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				logger.error("Error Message: " + e.getMessage());
			}
		};
		return callback;
	}

	private static void parse(byte[] bytes, String timeStamp, Map<String, List<Data>> userTransactionData) {
		BufferedReader input = ParsingUtils.readTextFromHtml(bytes);
		try {
			String inputLine;
			Data data = new Data();
			data.setDataType(AMAZON_DELIVERED);
			while ((inputLine = input.readLine()) != null) {
				if (inputLine.indexOf("Shipment Total") >= 0) {
					Matcher m = p.matcher(inputLine);
					if (m.find()) {
						String cr = m.group(0);
						NumberUtils.toFloat(cr);
						data.addProperty(ORDER_TOTAL, cr);
						break;
					}

				}else if(inputLine.indexOf("Hello ")>=0) {
					data.addProperty(NAME, inputLine.substring(inputLine.indexOf("Hello ")+6, inputLine.indexOf(",")));
				}
			}

			data.addProperty(DATE, timeStamp);
			List<Data> list = userTransactionData.get(AMAZON_DELIVERED);
			if (list == null)
				list = Lists.newArrayList();
			list.add(data);
			userTransactionData.put(AMAZON_DELIVERED, list);
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
