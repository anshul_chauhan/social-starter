package com.lembas.social.parsers;

import static com.lembas.constants.Constants.AMAZON_RETURNED;
import static com.lembas.constants.Constants.DATE;
import static com.lembas.constants.Constants.INTERNAL_DATE;
import static com.lembas.constants.Constants.LIMIT;
import static com.lembas.constants.Constants.ORDER_TOTAL;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Lists;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.lembas.social.Data;

public class AmazonReturnParser {
	private static final String SHIPMENT_EMAIL = "from:payments-messages@amazon.in subject:(\"Refund\")";
	private final static Logger logger = LoggerFactory.getLogger(PaytmSummaryParser.class);
	private final static Pattern p = Pattern.compile("\\d{1,3}(,\\d{3})*(\\.\\d\\d)?", Pattern.DOTALL);

	public static void parseShipment(Gmail service, String user, Map<String, List<Data>> userTransactionData)
			throws IOException {
		BatchRequest batch = service.batch();
		ListMessagesResponse messageList = service.users().messages().list(user).setQ(SHIPMENT_EMAIL)
				.setMaxResults(LIMIT).execute();

		List<Message> messages = messageList.getMessages();
		for (Message message : messages) {
			service.users().messages().get(user, message.getId()).queue(batch,
					getShipmentCallback(System.currentTimeMillis(), userTransactionData));
		}
		batch.execute();
	}

	private static JsonBatchCallback<Message> getShipmentCallback(final long currentTimeMillis,
			final Map<String, List<Data>> userTransactionData) {
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				MessagePart payload = message.getPayload();
				List<MessagePart> parts = payload.getParts();
				for (MessagePart part : parts) {

					if (part.getMimeType().equals("text/plain")) {

						parse(part.getBody().decodeData(), (String) message.get(INTERNAL_DATE), userTransactionData);

					}
				}
			}

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				logger.error("Error Message: " + e.getMessage());
			}
		};
		return callback;
	}

	private static void parse(byte[] bytes, String timeStamp, Map<String, List<Data>> userTransactionData) {
		String message = new String(bytes);
		String str = "Your refund is being credited as follows:";
		int endIndexOfStr = message.indexOf(str) + str.length();
		int indexOfSep = message.indexOf(":", endIndexOfStr);
		String paymentMethod = message.substring(endIndexOfStr, indexOfSep);
		paymentMethod = paymentMethod.trim();
		int beginIndex = message.indexOf("Rs.", indexOfSep) + 3;
		String orderTotal = message.substring(beginIndex, message.indexOf(" ", beginIndex));
		Matcher m = p.matcher(orderTotal);
		if (m.find()) {
			String cr = m.group(0);
			orderTotal = cr;
		}
		Data data = new Data();
		data.setDataType(AMAZON_RETURNED);
		data.addProperty(ORDER_TOTAL, orderTotal);
		data.addProperty(DATE, timeStamp);
		List<Data> list = userTransactionData.get(AMAZON_RETURNED);
		if (list == null)
			list = Lists.newArrayList();
		list.add(data);
		userTransactionData.put(AMAZON_RETURNED, list);
		// BufferedReader input = ParsingUtils.readTextFromHtml(bytes);
		// try {
		// String inputLine;
		// Data data = new Data();
		// data.setDataType(AMAZON_RETURNED);
		// while ((inputLine = input.readLine()) != null) {
		// if (inputLine.indexOf(str) >= 0) {
		// Matcher m = p.matcher(inputLine);
		// if (m.find()) {
		// String cr = m.group(0);
		// NumberUtils.toFloat(cr);
		// data.addProperty(ORDER_TOTAL, cr);
		// break;
		// }
		//
		// }
		// }
		//
		// data.addProperty(DATE, timeStamp);
		// List<Data> list = userTransactionData.get(AMAZON_RETURNED);
		// if (list == null)
		// list = Lists.newArrayList();
		// list.add(data);
		// userTransactionData.put(AMAZON_RETURNED, list);
		// input.close();
		// } catch (Exception e) {
		// e.printStackTrace();
		// } finally {
		// try {
		// input.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
	}
}
