package com.lembas.social.parsers;

import static com.lembas.constants.Constants.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.googleapis.batch.BatchCallback;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonErrorContainer;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Base64;
import com.google.api.client.util.Lists;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.lembas.social.Data;
import com.lembas.social.ParsingUtils;

public class PaytmOrder {
	private static final String ORDER_DELIVERED_EMAIL = "from:no-reply@paytm.com has:attachment to:me";
	private final static Logger logger = LoggerFactory.getLogger(SnapdealParser.class);
	private static Pattern p = Pattern.compile("[0-9]*\\.?[0-9]+", Pattern.DOTALL);

	public static void parseOrder(Gmail service, String user, Map<String, List<Data>> userTransactionData)
			throws IOException {
		BatchRequest batch = service.batch();
		ListMessagesResponse messageList = service.users().messages().list(user).setQ(ORDER_DELIVERED_EMAIL)
				.setMaxResults(LIMIT).execute();

		BatchRequest attachBatch = service.batch();
		List<Message> messages = messageList.getMessages();
		if (messages != null) {
			for (Message message : messages) {
				service.users().messages().get(user, message.getId()).queue(batch,
						getSnapdealOrderCallback(service, userTransactionData, attachBatch));
			}
			batch.execute();
			attachBatch.execute();
		}
	}

	private static JsonBatchCallback<Message> getSnapdealOrderCallback(final Gmail service,
			final Map<String, List<Data>> userTransactionData, BatchRequest attachBatch) {
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				MessagePart payload = message.getPayload();
				List<MessagePart> parts = payload.getParts();
				for (MessagePart part : parts) {
					if (part.getMimeType().equals("application/pdf")) {

						if (part.getFilename() != null && part.getFilename().length() > 0) {
							String attId = part.getBody().getAttachmentId();

							try {
								service.users().messages().attachments().get("me", message.getId(), attId).queue(
										attachBatch, GoogleJsonErrorContainer.class, getSnapdealInvoiceCallback(service,
												userTransactionData, (String) message.get(INTERNAL_DATE)));

							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
			};

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				logger.error("Error Message: " + e.getMessage());
			}
		};
		return callback;
	}

	private static BatchCallback<MessagePartBody, GoogleJsonErrorContainer> getSnapdealInvoiceCallback(
			final Gmail service, final Map<String, List<Data>> userTransactionData, String date) {
		BatchCallback<MessagePartBody, GoogleJsonErrorContainer> callback = new BatchCallback<MessagePartBody, GoogleJsonErrorContainer>() {

			@Override
			public void onSuccess(MessagePartBody attachPart, HttpHeaders responseHeaders) throws IOException {
				byte[] bytes = Base64.decodeBase64(attachPart.getData());
				parseOrder(bytes, date, userTransactionData);

			}

			@Override
			public void onFailure(GoogleJsonErrorContainer e, HttpHeaders responseHeaders) throws IOException {
				logger.error("Error Message: " + e.getError().getMessage());

			}

		};
		return callback;
	}

	private static void parseOrder(byte[] bytes, String timeStamp, Map<String, List<Data>> userTransactionData) {
		BufferedReader input = ParsingUtils.readTextFromPdf(bytes);
		try {
			String inputLine;
			Data data = new Data();
			data.setDataType(PAYTM_DELIVERED);
			while ((inputLine = input.readLine()) != null) {
				inputLine = inputLine.toLowerCase();
				if (inputLine.indexOf("grand total") >= 0 || inputLine.indexOf("total paid") >= 0) {
					Matcher m = p.matcher(inputLine);
					if (m.find()) {
						String cr = m.group(0);
						data.addProperty(ORDER_TOTAL, cr);
						break;
					}

				}
			}
			data.addProperty(DATE, timeStamp);
			List<Data> list = userTransactionData.get(PAYTM_DELIVERED);
			if (list == null)
				list = Lists.newArrayList();
			list.add(data);
			userTransactionData.put(PAYTM_DELIVERED, list);

			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
