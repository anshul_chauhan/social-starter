package com.lembas.social.parsers;

import static com.lembas.constants.Constants.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.util.Lists;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.lembas.social.Data;

public class FlipkartReturnParser {
	private static final String SHIPMENT_EMAIL = "from:no-reply@flipkart.com subject:(\"Refund\")";
	private final static Logger logger = LoggerFactory.getLogger(PaytmSummaryParser.class);
	private final static Pattern p = Pattern.compile("\\d{1,3}(,\\d{3})*(\\.\\d\\d)?", Pattern.DOTALL);

	public static void parseShipment(Gmail service, String user, Map<String, List<Data>> userTransactionData)
			throws IOException {
		BatchRequest batch = service.batch();
		ListMessagesResponse messageList = service.users().messages().list(user).setQ(SHIPMENT_EMAIL)
				.setMaxResults(LIMIT).execute();

		List<Message> messages = messageList.getMessages();
		for (Message message : messages) {
			service.users().messages().get(user, message.getId()).queue(batch,
					getParserCallBack(System.currentTimeMillis(), userTransactionData));
		}
		batch.execute();
	}

	private static JsonBatchCallback<Message> getParserCallBack(final long currentTimeMillis,
			final Map<String, List<Data>> userTransactionData) {
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				MessagePart payload = message.getPayload();
				if(payload.getMimeType().equals("text/html")) {
					
					parse(payload.getBody().decodeData(), (String) message.get(INTERNAL_DATE), userTransactionData);
				}
			}

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				logger.error("Error Message: " + e.getMessage());
			}
		};
		return callback;
	}

	private static void parse(byte[] bytes, String timeStamp, Map<String, List<Data>> userTransactionData) {
		String message = new String(bytes);
		String startString = "The amount of Rs.";
		String stopString = "towards";
		int endIndexOfStr = message.indexOf(startString) + startString.length();
		int startIndexOfStopString = message.indexOf(stopString, endIndexOfStr);
		String orderTotal = message.substring(endIndexOfStr, startIndexOfStopString);
		orderTotal = orderTotal.trim();
		Data data = new Data();
		data.setDataType(FLIPKART_RETURNED);
		data.addProperty(ORDER_TOTAL, orderTotal);
		data.addProperty(DATE, timeStamp);
		List<Data> list = userTransactionData.get(FLIPKART_RETURNED);
		if (list == null)
			list = Lists.newArrayList();
		list.add(data);
		userTransactionData.put(FLIPKART_RETURNED, list);
		
	}
}
