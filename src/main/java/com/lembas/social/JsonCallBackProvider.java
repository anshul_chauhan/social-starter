package com.lembas.social;

import java.util.List;
import java.util.Map;

import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;

public class JsonCallBackProvider {
	public static JsonBatchCallback<Message> getPaytmSummaryCallBack(final long currentTimeMillis,
			final Map<String, List<Data>> userTransactionData) {
		JsonBatchCallback<Message> callback = new JsonBatchCallback<Message>() {

			public void onSuccess(Message message, HttpHeaders responseHeaders) {
				MessagePart payload = message.getPayload();
				List<MessagePart> parts = payload.getParts();
				for (MessagePart part : parts) {
					if (part.getMimeType().startsWith("multipart")) {
						List<MessagePart> messageParts = part.getParts();
						for (MessagePart msgPart : messageParts) {
							if (msgPart.getMimeType().equals("text/html")) {
								ParsingUtils.parsePaytmSusmmary(new String(msgPart.getBody().decodeData()), "test",
										userTransactionData);
							}
						}
					}
				}
			//	System.out.println("Total time by paytm Callback============>" + (System.currentTimeMillis() - currentTimeMillis));
			}

			public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) {
				System.out.println("Error Message: " + e.getMessage());
			}
		};
		return callback;
	}
}
