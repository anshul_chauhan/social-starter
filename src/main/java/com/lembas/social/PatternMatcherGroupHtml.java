package com.lembas.social;

public class PatternMatcherGroupHtml {
	public static String stringToSearch = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
			+ "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" + "<head>\n"
			+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
			+ "<title>Untitled Document</title>\n" + "<style type=\"text/css\">\n" + "@font-face {\n"
			+ "    font-family:'Open Sans'; font-style:normal; font-weight:600;\n"
			+ "    src:local('Open Sans Semibold'), local('OpenSans-Semibold'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/MTP_ySUJH_bn48VBG8sNSnhCUOGz7vYGh680lGh-uXM.woff) format('woff');}\n"
			+ "@font-face {\n" + "    font-family:'Open Sans'; font-style:normal; font-weight:300;\n"
			+ "    src:local('Open Sans Light'), local('OpenSans-Light'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/DXI1ORHCpsQm3Vp6mXoaTXhCUOGz7vYGh680lGh-uXM.woff) format('woff');}\n"
			+ "</style>\n" + "</head>\n" + "<body>\n"
			+ "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" + "  <tr>\n" + "    <td>\n"
			+ "      <table width=\"502\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n"
			+ "      <tr>\n" + "        <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n"
			+ "          <tr>\n" + "            <td colspan=\"2\">&nbsp;</td>\n" + "            </tr>\n"
			+ "          <tr>\n" + "            <td width=\"5%\">&nbsp;</td>\n"
			+ "            <td width=\"75%\"><img src=\"https://s3-ap-southeast-1.amazonaws.com/assets.paytm.com/promotion/paytmlogo.gif\" /></td>\n"
			+ "            <td width=\"20%\"><img src=\"https://s3-ap-southeast-1.amazonaws.com/assets.paytm.com/promotion/seal.png\" /></td>\n"
			+ "          </tr>\n" + "        </table></td>\n" + "      </tr>\n" + "      <tr>\n"
			+ "        <td>&nbsp;</td>\n" + "      </tr>\n" + "      <tr>\n"
			+ "        <td bgcolor=\"#f3f3f3\"><table width=\"90%\" style=\"margin-left:26px;\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">\n"
			+ "          <tr>\n" + "            <td height=\"30\">&nbsp;</td>\n" + "          </tr>\n"
			+ "          <tr>\n"
			+ "            <td style=\"font-family:'Open Sans', Arial, Helvetica, sans-serif; font-size:14px;\"> Hi Anshul, <br />\n"
			+ "              Your <strong>Paytm Wallet Statement for February 2016 </strong> is here.</td>\n"
			+ "          </tr>\n" + "          <tr>\n" + "            <td>&nbsp;</td>\n" + "          </tr>\n"
			+ "          <tr>\n"
			+ "            <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:solid 1px #dbdbdb;\">\n"
			+ "              \n" + "              <tr>\n" + "                <td bgcolor=\"#FFFFFF\" >&nbsp;</td>\n"
			+ "                <td bgcolor=\"#FFFFFF\"><table style=\"padding: 10px;\" width=\"96%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\">\n"
			+ "                  <tr>\n"
			+ "                    <td colspan=\"2\" style=\"font-family:'Open Sans', Arial, sans-serif; font-size:10px; color:#9a9a9a;\">ACCOUNT DETAILS</td>\n"
			+ "                    </tr>\n" + "                  <tr>\n"
			+ "                    <td colspan=\"2\" height=\"5\"></td>\n" + "                  </tr>\n"
			+ "                  <tr>\n"
			+ "                    <td style=\"font-family:'Open Sans', Arial, sans-serif; font-size:13px;\"> +91-9717319555 </td>\n"
			+ "                    <td align=\"right\" style=\"font-family:'Open Sans', Arial, sans-serif; font-size:13px;\"><strong>Paytm Balance Rs 2091.47 </strong></td>\n"
			+ "                  </tr>\n" + "                  <tr>\n"
			+ "                    <td style=\"font-family:'Open Sans', Arial, sans-serif; font-size:13px;\"> iwalkalone7@gmail.com </td>\n"
			+ "                    <td align=\"right\" style=\"font-family:'Open Sans', Arial, sans-serif; font-size:11px; color:#999;\">(as on 29 February, 2016)</td>\n"
			+ "                  </tr>                  \n" + "                </table></td>\n"
			+ "                <td bgcolor=\"#FFFFFF\">&nbsp;</td>\n" + "              </tr>\n" + "              \n"
			+ "            </table></td>\n" + "          </tr>\n" + "          <tr>\n" + "            <td>&nbsp;</td>\n"
			+ "          </tr>\n" + "          <tr>\n"
			+ "            <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border:solid 1px #dbdbdb;\" >\n"
			+ "                      <tr>\n" + "                <td bgcolor=\"#FFFFFF\">&nbsp;</td>\n"
			+ "                <td bgcolor=\"#FFFFFF\"><table style=\"padding: 10px;\" width=\"96%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"0\" >\n"
			+ "                  <tr>\n"
			+ "                    <td colspan=\"2\" style=\"font-family:'Open Sans', Arial, sans-serif; font-size:10px; color:#9a9a9a;\">PAYTM WALLET SUMMARY - FEBRUARY 2016 </td>\n"
			+ "                  </tr>\n" + "                  <tr>\n"
			+ "                    <td colspan=\"2\" height=\"5\"></td>\n" + "                  </tr>\n"
			+ "                  <tr>\n"
			+ "                    <td width=\"64%\" style=\"font-family:'Open Sans', Arial, sans-serif; font-size:13px;\">Opening balance <span style=\"font-size:11px; color:#999;\">(as on 01 February, 2016)</span></td>\n"
			+ "                    <td width=\"36%\" align=\"right\" style=\"font-family:'Open Sans', Arial, sans-serif; color:#61900c; font-size:12px;\"><strong>&#43; Rs 0.2 </strong></td>\n"
			+ "                  </tr>\n" + "                  <tr>\n"
			+ "                    <td style=\"font-family:'Open Sans', Arial, sans-serif; font-size:13px;\">Total Amount credited </td>\n"
			+ "                    <td width=\"36%\" align=\"right\" style=\"font-family:'Open Sans', Arial, sans-serif; color:#61900c; font-size:12px;\"><strong>&#43; Rs 7700 </strong></td>\n"
			+ "                  </tr>\n" + "                  <tr>\n"
			+ "                    <td style=\"font-family:'Open Sans', Arial, sans-serif; font-size:13px;\">Total Amount debited </td>\n"
			+ "                    <td width=\"36%\" align=\"right\" style=\"font-family:'Open Sans', Arial, sans-serif; color:#bb0f1d; font-size:12px;\"><strong>- Rs 5609.33 </strong></td>\n"
			+ "                  </tr>\n" + "                </table></td>\n"
			+ "                <td bgcolor=\"#FFFFFF\">&nbsp;</td>\n" + "              </tr>\n" + "              \n"
			+ "            </table></td>\n" + "          </tr>\n" + "          <tr>\n" + "            <td>&nbsp;</td>\n"
			+ "          </tr>\n" + "          \n" + "          <tr>\n"
			+ "            <td align=\"center\" style=\"font-family:'Open Sans', Arial, Helvetica, sans-serif; font-size:15px;\">Now you can <strong>Pay with Paytm</strong> at following <a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxFqa3VKwZKrRYAEF1U1FQFFSMP5GcdSsIS6GL6kHaOKd_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Voxrx0VIepjJRLH55fV8UCD6EG8slmFqfKr-2FyIDfsF69TuVjiHksi4tDv6gdodaXo1kbjDVuJ2ANcpaY3wfpD-2BFRwZf-2BnPfCBfnW595EwLSRm-2Fhd4fSD3GVKx8YopuiQkDAl-2FA-2FQMlTxwcpMtz9uI7u6Q-3D-3D\">places  </a> </td>\n"
			+ "          </tr>\n" + "         \n" + "          <tr>\n" + "            <td>&nbsp;</td>\n"
			+ "          </tr>\n" + "          <tr>\n" + "            <td>\n"
			+ "              <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\" border-collapse:collapse;\">\n"
			+ "                <tbody>\n" + "                  <tr>\n"
			+ "                      <td align=\"center\" ><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxIjZd1Uf9PnKcocjpNJPXzsuKzUyP6vdf5JtwHITjrkFtpzdnBHrZlYNlfKQTgzXHtIwv3AB4sx6TSED9qBbFC8-3D_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8VoxAJYYXCYCsTE3ezw12ack5DllOzCiyWXVzU7nlHvm739-2Fcukucx-2B-2FMfnGIO32W-2FGu-2F-2F5UpZmysYA2fvXz-2F7Ziv4aX-2BtzIcnDY-2F4lROO1Q-2B3Kbth-2BJt3MJGSRvxCULIcmd-2FVnqGSMXYnWCtM4zdebmGQ-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/450x178_feb_2016.jpg\" style=\"border: 1px solid #ccc;\" /></a></td>\n"
			+ "                  </tr>\n" + "            <!--<tr>\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxCdxO-2Fz8SVkBOTFcLKkRNCNPbxhnlMivmTc5b-2B7nhkl3_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Voxos9VgQDDGaMpetb4Spz0oY8UkuuGckL0erVUZ8nAuXpLH6UiLtYUN970KM0D-2Fn0yIN1bqNF5X1EdMgYpiV0TIoQpuzhrd0Thr0gf4lV6rt5VkV6yCuMwVV3GfuPrADqzMRZMhFO5x4tuU3UW82DSyQ-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/firstcry_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxGY-2BXaUYVYpOcNneW1411Yp-2F5Mqegw-2B6HlJkkoLgSeDs_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8VoxloWL7lTXL2zOgyjTW1hsKpCl7UOnEfjT-2F5VGMvg0XmaMTv90tGbgX3ptqUDBgECjl7t6ZRmHCQ4413KXvchsi8BbXVvKPqmQBea69U-2FaHrIpIXdBULITZ-2Fu5xDZbgA0ymfaiPeGmMrT3ZrDowPTHFQ-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/zoomin_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxOF78O3lx9JfOAkpFu11A-2B-2BS6sH4pQ2nkW4sPetAwZiu_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Vox4mDDLAsV864A6qjT-2BTJ6MuyvoQ84lOQe6w5qmYJH46yjOg3lZjGyywaXIyXPtmsi-2FAWuR5wNJyrCC6zJvoPvEQbdffzOyQf-2Bd03gysen9PRvYT-2FZtD9h91F8zzdir21LRuQbK1HiyLO8Kh3nSOG1EQ-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/printvenue_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxAWPE0N4j2ddfSFjrH1JYDUR-2BKNeYJL23XFgU9U4NHGE6cY8pCf8Y4gkmMqK2oaiFQ-3D-3D_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Vox0nLePkdWAnGgpivqlPzuAGOJkenwOYYgU9IEgXjTXd5wJylKDUdkQ3RdkWKbzkNJOw2009mmMRekPyAgnzuJWDjixdSb-2BP1pkAn4kYNpk5oUHmeAbJFKbaDj-2FL6eJ-2Ft3Cn7Q7poOKetrHTvqt1Lqzw-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/magzter_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "                </tr>\n" + "                \n" + "                <tr style=\"margin-top:10px;\">\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxIHAkmTHZgMC7vn3VhMfVOifMFG-2F587iekhsmx896wqj_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Vox1pzBSBfFfy0UJfT-2F40kFGaSNcuwHAfyRqupTd56Ddnjdne-2BAO0mKLiV5X8ALtZ4w2y4eZL55FIQe661Tx4kb4hyW04IVMCC1BjcEDfkxmo-2FAtOUEXHESy9xSRmgRvZJYTXJyl-2FfKd-2FPZ75XUbqtiaw-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/tolexo_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxCdxO-2Fz8SVkBOTFcLKkRNCOM-2BbWSnTyqbWLeKqpoWcNGWlYr4YWxad4QNvmNDQeZkw-3D-3D_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8VoxelZlEN1Fhfm2hcDlPDSzO-2Bfpd3c6-2B3gvZGXe2gEdwGTldeP-2BXAfv5pdj8EQJwddiQwNnkoPO4r8OtIHnTkiPIpfYRcFmWuul2KY5GVQkiiaNGDd8hsjCEVa3gIJ7C36eZBgWaG5nWl0vVmdoljaf4w-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/foodpanda_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxLbQtxfzCRFRz5aRPrqOyJDBTRWmToTaVN2FTZqIwxEJO2-2B1A8Lvn980MKAJZRbGzQ-3D-3D_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8VoxxzCXxG39-2FEOWRMOqtfje67G-2BR8vj7pr0OcZTsuPrZmWbepojgS2dORM1nBZe7H5RseQ2dAGXd0MUp7d9Tid9s1di1d2hmVoapdy-2BgmlKszuFQciAnVUv4J3G1SVe1gq-2BFS89rYrbmchfgWUjzbnYKA-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/jabong_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "\n"
			+ "                  <td align=\"center\" style=\"border: 1px solid #ccc;\"><a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxIHAkmTHZgMC7vn3VhMfVOg9LSb3y-2BdnLv1Yn9aaLzVP_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8VoxySwqE-2BMx2nz5w4sp-2FZ0U30gd4VxASjj1wsKNSrNK3ZzH8OSOTgYXo9M3h7z7FmNXlaMsQpiQ6ZIKUhgtjueEt8NqNB-2BMC91Gf02mfobeORQ2Hz7dgFq9kW5L9Romq6bpiQ8OJhmridkfvJiB7YDf7Q-3D-3D\"><img src=\"http://assetscdn.paytm.com/promotion/tinyowl_june2015.jpg\" style=\"padding: 1px;\"></a></td>\n"
			+ "                </tr> -->\n" + "              </tbody></table>\n" + "            </td>\n"
			+ "          </tr>\n" + "         <!--  <tr>\n" + "            <td>&nbsp;</td>\n" + "          </tr>\n"
			+ "	<tr>\n"
			+ "            <td><strong style=\"font-family: 'Open Sans';font-size: 13px;\">You can transfer money from your Paytm wallet to your friends, download our latest and awesome <a href=\"http://email.paytm.com/wf/click?upn=7QA1vd7021-2B5a18UJ-2FLKStRQ1Sol4R8seCVner-2FTgLE-3D_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Voxt3v5IGY1c2hBJXdYozpM4Y5Myu0vAn22kPsKjeB0JLjGIymvy0u3Bst819-2B9iAZujIHgPzLpvuTKVzN-2BBeDfIFJH5sCfktLYP8yu1pSn3vjcQEDE9ScwFqVdveocEipu6IOCCsokCJs0gOfwemB6Tq8UwQV4rzJnqzWn7wisRu4-3D\">Paytm Wallet app</a></strong></td>\n"
			+ "          </tr> -->\n" + "         <!--   <tr>\n" + "            <td>&nbsp;</td>\n" + "          </tr>\n"
			+ "          <tr>\n" + "            <td align=\"center\" >\n"
			+ "              <a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxIHAkmTHZgMC7vn3VhMfVOhYHfEXHR6mXK6K5B2nGYLN_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Vox-2FYjGgFtweXxWjv-2BWpNqPbWE9w0SorHGJXEGzAGC0IpOosvuBoi-2BRfroOZFT-2B3-2B7JOXXCefxa9JQajPGJRiJHHAUiLPgXd2mY8B5kOal2QJmEi4T-2BErtd-2BmJvD-2FCHB3LmQTLpWICQb1tChEB1P4BxsB-2BeWv9-2FivKvCCWU-2ByButU4-3D\" style=\"outline:0; text-decoration:none; border:none;\"><img src=\"http://assetscdn.paytm.com/promotion/450x88_june2015.jpg\" /></a></td>\n"
			+ "          </tr> -->\n" + "	<tr>\n" + "            <td>&nbsp;</td>\n" + "          </tr>\n" + "\n"
			+ "          <tr>\n" + "            <td height=\"1\" bgcolor=\"#e4e4e4\"></td>\n" + "          </tr>\n"
			+ "          \n" + "          <tr>\n" + "            <td>\n" + "\n" + "              </td>\n"
			+ "          </tr>\n" + "            </table></td>\n" + "          </tr>\n" + "          \n"
			+ "          <tr>\n" + "            <td height=\"1\" bgcolor=\"#e4e4e4\"></td>\n" + "          </tr>\n"
			+ "          <tr>\n" + "            <td>&nbsp;</td>\n" + "          </tr>\n" + "          <tr>\n"
			+ "            <td align=\"left\" style=\"font-family:'Open Sans', Arial, Helvetica, sans-serif; font-size:13px;\">For queries visit us at <a  href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxF1J2GznYa1rPGOMtWMvOJI-3D_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Vox72D5bFu7NfOx-2FNRADCi5cKfdA8HfFe-2FuXIWRIcxCB5ZFKG-2BCA6NIRYOkScs2789dvvIoqyEh-2BL7G8PwlijJSNJRzWFxYVBpVwmFPPxhwrf4ka5SnpjB9UQ6-2Fk2Iq-2B-2FaKzxZA420tXiwk-2BwARNWJvmjCPsxmOddGOIoAZiGHgPxA-3D\">https://paytm.com/care</a><br><br>\n"
			+ "What is Paytm Wallet?</strong><br>\n" + "\n"
			+ "Paytm Wallet is RBI approved semi closed wallet which allows you to store money. Money stored in Paytm Wallet is secure and can be used to recharge, pay your bills or buy any item at Paytm or at other merchant sites where it has been seamlessly integrated with Paytm. <a href=\"http://email.paytm.com/wf/click?upn=IKuVQxBFPo7wLgUI3YPlxBlP3PYo8x-2FuDx-2BylZ1Jhm1qdnTfI4QtQn5mNIGd1X7h_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Voxy8VAx6W-2Br9TV6DsNAdgT0WoKreOvT7i4v0XTJz5wM-2FINqsBeDmwA-2FH1JmF7owJ60ticf-2FdTybRY9KSIvOcV-2Bx1hXVPd56VL9O90D0sDNYUBNBabtdiB-2BbRWv-2B-2F1yn1u0Qm-2BjUQVpXz2fXzlTA47ke4853LRcWay9EumNE3SDieg-3D\" style=\"color:#029fce; text-decoration:none;\"> FAQs</a>.</td>\n"
			+ "          </tr> \n" + "          <tr>\n"
			+ "            <td style=\"font-family:'Open Sans', Arial, Helvetica, sans-serif; font-size:13px;\">\n"
			+ "              <br>\n"
			+ "              If you did not create this account on Paytm and you think someone else has used your Email ID to create an account, please remove your Email ID from this account by <a href=\"http://email.paytm.com/wf/click?upn=P9YCkL9mgAAXmpO3ESBovpmaYxd7ACEccV42Pjc8iN6BmUdez3fuypFN3KC0skU5_l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8Voxpu8Mt4Gjh4yGfo0TaVgYSh1Vb6g4Zn525f-2Bd8fbEcbSFMe53cew1-2BZ1PXSwYL65ZXO5epK7UprEpuEpkbX9dylKAFooQpEi-2FYDDgdNP05R2ok2BOa4cIOU1u0JvUi3-2FOl2S2nr6To9Gjw7-2BcYmVNgwcfA8raohVqYnH-2FouRH5iQ-3D\">clicking here</a>.\n"
			+ "            </td>\n" + "          </tr>\n" + "          <tr>\n"
			+ "            <td height=\"30\">&nbsp;</td>\n" + "          </tr>\n" + "        </table></td>\n"
			+ "      </tr>\n" + "    </table></td>\n" + "  </tr>\n" + "</table>\n" + "\n"
			+ "<img src=\"http://email.paytm.com/wf/open?upn=l8fbvsvLTGMstoTkiJBPZq5Zne2Dyv-2F-2FfWJWD9ZPFsYbjgL2EW3DYGO8OcZj8VoxQ9Xo7jNCwbguaI5FCJyHokyWTuOGXBhsZNXxDhT7glah6OiOhxoFXY7yOteIkPbbv8cqjpGS-2FG-2FMw6UbhJvOWDKB7QEji6LiFyc8X67lsqqBTImq5-2FEyxOib-2Fn0ePLO807E6d6JREyUuuOp5w9TQoijkGwwv8O15BtdvyvRff0E-3D\" alt=\"\" width=\"1\" height=\"1\" border=\"0\" style=\"height:1px !important;width:1px !important;border-width:0 !important;margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;\"/>\n"
			+ "</body>\n" + "</html>\n" + "\n" + "";

	public static void main(String[] args) {

		// String s1 = "12 Voorbeeld3 4Voorbeeld5 Total Amount credited
		// </td>\\n\" + \n"
		// + " \" <td width=\\\"36%\\\" align=\\\"right\\\"
		// style=\\\"font-family:'Open Sans', Arial, sans-serif; color:#61900c;
		// font-size:12px;\\\"><strong>&#43; Rs 7700 </strong> lsamdmf olj sdom
		// sodmd d 6.0 test 777";
		// Pattern p =
		// Pattern.compile("(credited|debited).*?\\s?[0-9]*\\.?[0-9]+\\s",
		// Pattern.DOTALL);
		/*
		 * Time comparison between 3 methods: indexOf is run fastest in our case
		 * as the string we are searching for is not repeated partially, which
		 * makes indexOf fast as it runs on KMP. Parser is slow for obvious
		 * reasons.
		 */
		// patternMatcher(stringToSearch, p);
		javaIndexOf(stringToSearch);
		// parseHtml(stringToSearch);
	}

	// private static void parseHtml(String stringToSearch) {
	// long currentTimeMillis = System.currentTimeMillis();
	// for (int i = 0; i < 100; i++) {
	// Document parse = Jsoup.parse(stringToSearch);
	// stringToSearch = parse.body().text();
	// int indexOf = stringToSearch.indexOf("Rs ",
	// stringToSearch.indexOf("credited"));
	// String cr = stringToSearch.substring(indexOf + 3,
	// stringToSearch.indexOf(" ", indexOf + 3));
	// int indexOf1 = stringToSearch.indexOf("Rs ",
	// stringToSearch.indexOf("debited"));
	// String db = stringToSearch.substring(indexOf1 + 3,
	// stringToSearch.indexOf(" ", indexOf1 + 3));
	// }
	// System.out.println("Total time by parser============>" +
	// (System.currentTimeMillis() - currentTimeMillis));
	// }
	//
	// private static void patternMatcher(String stringToSearch, Pattern p) {
	// // the pattern we want to search for
	//
	// long currentTimeMillis = System.currentTimeMillis();
	//
	// Matcher m = p.matcher(stringToSearch);
	// // if we find a match, get the group
	// for (int i = 0; i < 100; i++) {
	// while (m.find()) {
	// // get the matching group
	//
	// String codeGroup = m.group(0);
	// codeGroup = codeGroup.substring(codeGroup.lastIndexOf(" ",
	// codeGroup.length() - 2) + 1);
	// // print the group
	// System.out.println(codeGroup);
	// }
	// }
	// System.out.println("Total time by matcher============>" +
	// (System.currentTimeMillis() - currentTimeMillis));
	// }

	private static void javaIndexOf(String stringToSearch) {
		long currentTimeMillis = System.currentTimeMillis();
		// uncomment the following code to check indexOf worst case
		// StringBuilder builder = new StringBuilder();
		// for (int i = 0; i < 1000; i++) {
		//
		// builder.append("credites");
		// builder.append("debites");
		// }
		// builder.append(s1);
		// stringToSearch = builder.toString();
		for (int i = 0; i < 100; i++) {

			int indexOfCreditedRs = stringToSearch.indexOf("Rs ", stringToSearch.indexOf("credited"));
			int secondLastIndexOfSpace = stringToSearch.indexOf(" ", indexOfCreditedRs + 3);
			String cr = stringToSearch.substring(indexOfCreditedRs + 3, secondLastIndexOfSpace);
			int indexOfDebitedRs = stringToSearch.indexOf("Rs ",
					stringToSearch.indexOf("debited", secondLastIndexOfSpace));
			String db = stringToSearch.substring(indexOfDebitedRs + 3,
					stringToSearch.indexOf(" ", indexOfDebitedRs + 3));
			// System.out.println("credit "+cr +" "+db);
		}
		System.out.println("Total time by indexOf============>" + (System.currentTimeMillis() - currentTimeMillis));
	}
}
