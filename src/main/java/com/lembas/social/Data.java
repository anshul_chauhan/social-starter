package com.lembas.social;

import java.util.Map;

import com.google.api.client.util.Maps;

public class Data {
	String dataType;
	Map<String, String> properties = Maps.newHashMap();

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	public String addProperty(String key, String value) {
		return properties.put(key, value);
	}

	public String removeProperty(String key) {
		return properties.remove(key);
	}

	public String toString() {
		return dataType + " : " + properties;
	}
}
