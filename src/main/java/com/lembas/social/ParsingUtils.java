package com.lembas.social;

import static com.lembas.constants.Constants.DATE;
import static com.lembas.constants.Constants.PAYTM_SUMMARY;
import static com.lembas.constants.Constants.PAYTM_SUMMARY_CREDITED;
import static com.lembas.constants.Constants.PAYTM_SUMMARY_DEBITED;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BodyContentHandler;

import com.google.api.client.util.Lists;

public class ParsingUtils {
	static Metadata metadata = new Metadata();
	static ParseContext pcontext = new ParseContext();
	static HtmlParser htmlparser = new HtmlParser();
	static Pattern p = Pattern.compile("[0-9]*\\.?[0-9]+", Pattern.DOTALL);
	static HttpClient httpClient = new DefaultHttpClient();

	public static void parsePaytmSusmmary(String stringToSearch, String timeStamp,
			Map<String, List<Data>> userTransactionData) {

		int indexOfCreditedRs = stringToSearch.indexOf("Rs ", stringToSearch.indexOf("credited"));
		int secondLastIndexOfSpace = stringToSearch.indexOf(" ", indexOfCreditedRs + 3);
		String cr = stringToSearch.substring(indexOfCreditedRs + 3, secondLastIndexOfSpace);
		int indexOfDebitedRs = stringToSearch.indexOf("Rs ", stringToSearch.indexOf("debited", secondLastIndexOfSpace));
		String db = stringToSearch.substring(indexOfDebitedRs + 3, stringToSearch.indexOf(" ", indexOfDebitedRs + 3));
		Data data = new Data();
		data.setDataType(PAYTM_SUMMARY);
		data.addProperty(PAYTM_SUMMARY_CREDITED, cr);
		data.addProperty(PAYTM_SUMMARY_DEBITED, db);
		data.addProperty(DATE, timeStamp);
		List<Data> list = userTransactionData.get(PAYTM_SUMMARY);
		if (list == null)
			list = Lists.newArrayList();
		list.add(data);
		userTransactionData.put(PAYTM_SUMMARY, list);

	}

	public static void parsePaytmSummmaryHtml(byte[] bs, String timeStamp,
			Map<String, List<Data>> userTransactionData) {
		try {
			byte[] bytes = PatternMatcherGroupHtml.stringToSearch.getBytes();
			InputStream is = new ByteArrayInputStream(bytes);
			long length = bytes.length;

			String stringToSearch = parseTikeService(is, length);
			int indexOfCredited = stringToSearch.indexOf("credited");
			Matcher m = p.matcher(stringToSearch.substring(indexOfCredited, indexOfCredited + 20));
			Data data = new Data();
			data.setDataType(PAYTM_SUMMARY);
			if (m.find()) {
				String cr = m.group(0);
				data.addProperty(PAYTM_SUMMARY_CREDITED, cr);
			}
			int indexOfDebited = stringToSearch.indexOf("debited", indexOfCredited);
			m = p.matcher(stringToSearch.substring(indexOfDebited, indexOfDebited + 20));
			if (m.find()) {
				String db = m.group(0);
				data.addProperty(PAYTM_SUMMARY_DEBITED, db);
			}
			data.addProperty(DATE, timeStamp);
			List<Data> list = userTransactionData.get(PAYTM_SUMMARY);
			if (list == null)
				list = Lists.newArrayList();
			list.add(data);
			userTransactionData.put(PAYTM_SUMMARY, list);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String parseTikeService(InputStream is, long length) throws ClientProtocolException, IOException {

		StringBuilder result = new StringBuilder();
		HttpPut putRequest = new HttpPut("http://localhost:9998/tika");
		putRequest.addHeader("Content-Type", "text/html");
		putRequest.addHeader("Accept", "text/plain");
		InputStreamEntity entity = new InputStreamEntity(is, length);
		// FileEntity fileEntity = new FileEntity(new File(path));
		putRequest.setEntity(entity);
		HttpResponse response = httpClient.execute(putRequest);
		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
		String output;
		while ((output = br.readLine()) != null) {
			result.append(output);
		}
		br.close();
		return result.toString();
	}

	/*
	 * After so may methods, Java rest calls turned out be second fastest next to java indexOf.
	 * But as it's much more flexible and elegant solution, we'll proceed with this furter
	 */
	public static BufferedReader readTextFromHtml(byte[] bytes) {
		try {
			URL url = new URL("http://localhost:9998/tika");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Content-Type", "text/html");
			connection.setRequestProperty("Accept", "text/plain");
			OutputStream os = connection.getOutputStream();
			os.write(bytes);
			os.flush();
			os.close();
			BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			return input;
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	public static BufferedReader readTextFromPdf(byte[] bytes) {
		try {
			URL url = new URL("http://localhost:9998/tika");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Content-Type", "application/pdf");
			connection.setRequestProperty("Accept", "text/plain");
			OutputStream os = connection.getOutputStream();
			os.write(bytes);
			os.flush();
			os.close();
			BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			return input;
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
